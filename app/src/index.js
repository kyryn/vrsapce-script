const express = require('express')
const axios = require('axios')
const fs = require('fs')
// const cors = require('cors');
const app = express()
const port = 3000

const FILE_NAME = '/app/data/test.xml'
const FILE_NAME_KBELY = '/app/data/test_kbely.xml'

const WHITELIST = ['::ffff:192.168.30.248', '::ffff:185.5.69.28', '::ffff:93.99.190.65', '::ffff:185.5.71.85']
// app.use(cors({ origin: ["http://localhost:3000", "https://origin2.com"] }));

app.get('/', (req, res) => {
  res.send('Hello World!')
})

// app.use((req, res, next) => {
//   console.log('Time: ', Date.now())
//   console.log(req.ip, req.connection.remoteAddress)
//   const ip = req.connection.remoteAddress
//   if (!WHITELIST.includes(ip)) {
//     res.status(403).end('forbidden' + ip)
//   } else {
//     next()
//   }
// })

const SOAP_URL = 'http://193.105.192.64:8100/CRMGeneralWebService.asmx'
const SOAP_HEADERS = {
  'Authorization': 'None',
  'Content-Type': 'text/xml'
}
const SOAP_XMLS =
  '<?xml version="1.0" encoding="utf-8"?>' +
  '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">' +
  '<soap:Body>' +
  '<Retrieve xmlns="http://llpgroup.com/CRM/">' +
  '<entityList>' +
  '<entities>' +
  '<entity name="product">' +
  '<map crmField="llp_stav"/>' +
  '<map crmField="name"/>' +
  '<map crmField="llp_podlazifiltr"/>' +
  '<map crmField="llp_aktualnicenadlecenikusdph"/>' +
  '<map crmField="llp_aktualnicenadlecenikubezdph"/>' +
  '<map crmField="llp_plocha"/>' +
  '<map crmField="llp_dispozice"/>' +
  '<map crmField="llp_orientacefiltrglobal"/>' +
  '<filters field="llp_etapa" value="6cbc69d9-a4e3-eb11-bacb-0022489c0e34" operator="equals" crmType="Lookup"/>' +
  '<filters field="producttypecode" value="1" operator="equals" crmType="OptionSet"/>' +
  '</entity>' +
  '</entities>' +
  '</entityList>' +
  '</Retrieve>' +
  '</soap:Body>' +
  '</soap:Envelope>'

const SOAP_XMLS_KBELY =
  '<?xml version="1.0" encoding="utf-8"?>' +
  '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">' +
  '<soap:Body>' +
  '<Retrieve xmlns="http://llpgroup.com/CRM/">' +
  '<entityList>' +
  '<entities>' +
  '<entity name="product">' +
  '<map crmField="llp_stav"/>' +
  '<map crmField="name"/>' +
  '<map crmField="llp_podlazifiltr"/>' +
  '<map crmField="llp_aktualnicenadlecenikusdph"/>' +
  '<map crmField="llp_aktualnicenadlecenikubezdph"/>' +
  '<map crmField="llp_plocha"/>' +
  '<map crmField="llp_dispozice"/>' +
  '<map crmField="llp_orientacefiltrglobal"/>' +
  '<filters field="llp_etapa" value="4cfe815e-f883-ec11-8d21-0022489d245e" operator="equals" crmType="Lookup"/>' +
  '<filters field="producttypecode" value="1" operator="equals" crmType="OptionSet"/>' +
  '</entity>' +
  '</entities>' +
  '</entityList>' +
  '</Retrieve>' +
  '</soap:Body>' +
  '</soap:Envelope>'

app.get('/soap', (req, res) => {

  axios.post(SOAP_URL, SOAP_XMLS, { headers: SOAP_HEADERS, timeout: 5000 })
    .then(r => {
      console.log(r)
      //TODO vybar jen cast xmlka
      try {
        fs.writeFileSync(FILE_NAME, r.data)
        res.send('ok - file written successfully')
      } catch (err) {
        res.status(500).send('Error' + err.toString())
      }
    })
    .catch(err => {
      const e = JSON.stringify(err, null, 2)
      fs.writeFileSync('/app/data/log.txt','\n' +  ( new Date()).toUTCString() + '\n' + e, { flag: 'a+' })
      res.status(500).send('Error' + err.toString() + '<pre>' + e + '</pre>')
    })
})

app.get('/show', (req, res) => {
  try {
    const data = fs.readFileSync(FILE_NAME, 'utf8')
    res.type('application/xml').send(data)
  } catch (err) {
    res.status(500).send('Error' + err.toString())
  }
})

app.get('/kbely_soap', (req, res) => {

  axios.post(SOAP_URL, SOAP_XMLS_KBELY, { headers: SOAP_HEADERS, timeout: 5000 })
    .then(r => {
      console.log(r)
      //TODO vybar jen cast xmlka
      try {
        fs.writeFileSync(FILE_NAME_KBELY, r.data)
        res.send('ok - file written successfully')
      } catch (err) {
        res.status(500).send('Error' + err.toString())
      }
    })
    .catch(err => {
      const e = JSON.stringify(err, null, 2)
      fs.writeFileSync('/app/data/log.txt','\n' +  ( new Date()).toUTCString() + '\n' + e, { flag: 'a+' })
      res.status(500).send('Error' + err.toString() + '<pre>' + e + '</pre>')
    })
})

app.get('/kbely_show', (req, res) => {
  try {
    const data = fs.readFileSync(FILE_NAME_KBELY, 'utf8')
    res.type('application/xml').send(data)
  } catch (err) {
    res.status(500).send('Error' + err.toString())
  }
})

// app.get('/get_test', (req, res) => {
//   try {
//     axios.get('https://google.com')
//       .then(r => {
//         try {
//           fs.writeFileSync(FILE_NAME + 'test', r.data)
//           res.send('ok - file written successfully')
//         } catch (err) {
//           res.status(500).send('Error' + err.toString())
//         }
//       })
//       .catch(e => {
//         res.status(500).send('Error' + e.toString())
//       })
//   } catch (e) {
//     res.status(500).send('Error' + e.toString())
//   }
// })
//
// app.get('/show_test0', (req, res) => {
//   try {
//     res.type('application/xml').send(SOAP_XMLS)
//   } catch (err) {
//     res.status(500).send('Error' + err.toString())
//   }
// })
//
// app.get('/show_test1', (req, res) => {
//   try {
//     const data = fs.readFileSync(FILE_NAME + 'test', 'utf8')
//     res.type('application/xml').send(data)
//   } catch (err) {
//     res.status(500).send('Error' + err.toString())
//   }
// })

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
