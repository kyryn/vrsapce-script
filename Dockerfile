FROM node:16-slim

RUN apt update && apt install  openssh-server sudo -y

RUN useradd -rm -d /home/ubuntu -s /bin/bash -g root -G sudo -u 1001 test 
RUN usermod -aG root test
RUN echo 'test:test' | chpasswd
RUN service ssh start


# Create app directory
WORKDIR /app

RUN npm install -g nodemon

COPY app/package*.json ./

RUN npm install

EXPOSE 3000 22

ENTRYPOINT ["/bin/sh","-c","/usr/sbin/sshd && npm run watch"]